import React from "react";
import { geoLocationApiService } from '../services/api';
import { NavItem, InputState } from './navItem/navItem';
import { ToastContainer, toast } from 'react-toastify';
import GooogleMap from '../components/googleMap/googleMap'
export default class App extends React.Component {
    navOptions = [{ type: 'pickup', markerIcon: 'assets/dropOffMarker.svg', inputState: InputState.READY, placeholder: 'Pick up address', icons: { ready: "assets/pickUpBadgeBlank.svg", valid: "assets/pickUpBadgePresent.svg", error: "assets/pickUpBadgeError.svg" } },
    { type: 'dropoff', markerIcon: 'assets/pickUpMarker.svg', inputState: InputState.READY, placeholder: 'Drop off address', icons: { ready: "assets/dropOffBadgeBlank.svg", valid: "assets/dropOffBadgePresent.svg", error: "assets/dropOffBadgeError.svg" } }];
    btnOption = {
        isDisabled: true,
        defaultText: 'Create job',
        loadingText: 'Creating...'
    };
    isCreating = false;
    constructor(props) {
        super(props);
        const me = this;
        me.googleMap = React.createRef();
    }
    getGeoLocation = (address, navOption) => {
        // const address="29 Rue du 4 Septembre";
        let payload = { address: address };
        geoLocationApiService.getGeoLocation(payload).then(response => this.successGetGeoLocation(response, navOption)).catch(err => this.errorGetGeoLocation(err, navOption));
    }
    successGetGeoLocation(response, navOption) {
        const isValidAddress = response.data && response.data.latitude && response.data.longitude;
        navOption.inputState = isValidAddress ? InputState.VALID : InputState.ERROR;
        const position = {
            lat: response.data.latitude,
            lng: response.data.longitude
        }
        this.googleMap.current.AddMarker(navOption.markerIcon, position, navOption.type);
        this.forceUpdate();
    }
    errorGetGeoLocation(response, navOption) {
        navOption.inputState = InputState.ERROR;
        this.forceUpdate();
    }
    onInputBlur = (event) => {
        const address = event.target.value;
        const type = event.currentTarget.getAttribute('type');
        const navOption = this.navOptions.find(option => option.type == type);
        navOption.address = address;
        if (!address || address.length == 0) {
            navOption.inputState = InputState.READY;
            return this.forceUpdate();
        }
        this.getGeoLocation(address, navOption);
    }
    onCreateClick = () => {
        this.isCreating = true;
        const payload = {
            pickup: this.navOptions[0].address,
            dropoff: this.navOptions[1].address
        }
        geoLocationApiService.createJob(payload).then(response => {
            this.isCreating = false;
            toast('Job has been created successfully!');
            this.forceUpdate();
        });
        this.forceUpdate();
    }
    get isCreateBtnDisabled() {
        this.btnOption.isDisabled = this.navOptions.some(option => option.inputState != InputState.VALID);
        return this.btnOption.isDisabled || this.isCreating;
    }
    render() {
        return <div>
            <GooogleMap ref={this.googleMap}></GooogleMap>
            <div className="container">
                <ToastContainer position='top-right' autoClose={5000} />
                {this.navOptions.map(option => <NavItem key={option.type} type={option.type} onInputBlur={this.onInputBlur} icons={option.icons} inputState={option.inputState} placeholder={option.placeholder} />)}
                <button onClick={this.onCreateClick} className="create-btn" disabled={this.isCreateBtnDisabled} >{this.isCreating ? this.btnOption.loadingText : this.btnOption.defaultText}</button>
            </div>
        </div>;
    }
}