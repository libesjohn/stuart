import React from "react";
export default class GoogleMap extends React.Component {
    markers={};
    map;
    src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBRvMFhksDPYQ6mSea_vljgXpbXM4wxxqM'
    componentDidMount() {
        const script = document.createElement('script');
        script.src = this.src;
        script.defer = true;
        script.addEventListener('load', this.onScriptLoad);
        const firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(script, firstScriptTag);
    }
    onScriptLoad = () => {
        this.map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 48.86982,
                lng: 2.334579
            },
            mapTypeControl: false,
            zoom: 15
        });
    }
    AddMarker(icon, position, key) {
        const marker = new google.maps.Marker({
            position: position,
            icon: icon
        });

        marker.setMap(this.map);
    }
    render() {
        return <div id="map"></div>;
    }
}