import React from "react";
import { geoLocationApiService } from '../../services/api'
class NavItem extends React.Component {
    get iconSrc() {
        const { icons, inputState } = this.props;
        const icon = icons && inputState ? icons[inputState] : '';
        return icon;
    }
    render() {
        let { placeholder, onInputBlur, type } = this.props;
        return <div className="nav-option">
            <img className="nav-option-img" src={this.iconSrc} />
            <input type={type} className="nav-option-input" onBlur={onInputBlur} placeholder={placeholder} />
        </div>;
    }
}
const InputState = { READY: 'ready', VALID: 'valid', ERROR: 'error' }

export { InputState, NavItem }