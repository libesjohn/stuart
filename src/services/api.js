import axios from 'axios';
const url = {
    geoLocation: 'https://stuart­-frontend­-challenge.now.sh/geocode',
    jobs: 'https://stuart­-frontend­-challenge.now.sh/jobs'
};
class GeoLocationApiService {
    constructor() {
    }

    getGeoLocation(payload) {
        return axios.post(`${url.geoLocation}`, payload);
    }
    createJob(payload){
        return axios.post(`${url.jobs}`, payload);
    }
}
export const geoLocationApiService = new GeoLocationApiService();
